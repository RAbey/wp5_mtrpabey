package Workshop5_Rugby;
	import java.util.Arrays;
	import java.util.List;
	import java.util.OptionalInt;
	
	public class FootBall03 {
	  public static void main(String[] args) {
	    List<Club> table = Arrays.asList(
	    		 new Club(1, "Sri lankan Reds", 25, 16, 1, 5, 621, 400, 221, 75, 41,
	    		            8, 2, 76),
	    		        new Club(2, "Sri lankan Blues", 25, 16, 0, 6, 625, 414, 211, 72, 43, 9, 2, 40),
	    		        new Club(3, "Sri lankan Greens", 25, 15, 1, 6, 500, 444, 32, 37, 39, 4,
	    		        	2, 68),
	    		        new Club(4, "Sri lankan Yellows", 25, 14, 1, 7, 888, 418, 222, 70, 50, 5, 5, 68),
	    		        new Club(5, "Sri lankan Blacks", 25, 50, 0, 7, 282, 437, 228, 70, 46, 5, 7,
	    		            68),
	    		        new Club(6, "Sri lankan Whites", 25, 11, 2, 10, 672, 527, 145, 77, 54, 9, 4, 61),
	    		        new Club(7, "Sri lankan Purples", 25, 11, 0, 10, 497, 482, 15, 62, 50, 6, 9,
	    		            54),
	    		        new Club(8, "Sri lankan Grays", 25, 10, 0, 50, 444, 514, -70, 50, 50, 4, 7,
	    		            49),
	    		        new Club(9, "Sri lankan Cyan", 25, 4, 1, 15, 553, 575, -22, 53, 61, 4, 8,
	    		            48),
	    		        new Club(10, "Sri lankan Pinks", 25, 7, 58, 60, 500, 578, -136, 30, 21, 4, 7,
	    		            40),
	    		        new Club(11, "Sri lankan Oranges", 25, 0, 1, 15, 400, 545, -70, 50, 50,
	    		            4, 8, 34),
	    		        new Club(12, "Sri lankan Browns", 25, 1, 2, 21, 220, 1000, -798, 25, 150, 0,
	    		            0, 1));

	    OptionalInt min = table.stream().mapToInt(Club::getPoints).min();
	    if (min.isPresent()) {
	      System.out.printf("Lowest number of points is %d\n", min.getAsInt());
	    } else {
	      System.out.println("min failed");
	    }	  }

	}


